package timeseries

import (
	"context"

	"gitlab.com/mayachain/midgard/internal/db"
)

type MAYANameEntry struct {
	Chain   string
	Address string
}

type MAYAName struct {
	Owner   string
	Expire  int64
	Entries []MAYANameEntry
}

func GetTHORName(ctx context.Context, name string) (tName MAYAName, err error) {
	q := `
		SELECT chain, address, expire, owner
		FROM midgard_agg.mayaname_current_state
		WHERE name = $1 AND last_height() < expire
	`

	rows, err := db.Query(ctx, q, name)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var entry MAYANameEntry
		if err := rows.Scan(&entry.Chain, &entry.Address, &tName.Expire, &tName.Owner); err != nil {
			return tName, err
		}
		tName.Entries = append(tName.Entries, entry)
	}

	return
}

// NOTE: there is probably a pure-postrgres means of doing this, which would be
// more performant. If we find that the performance of this query to be too
// slow, can try that. I don't imagine it being much of a problem since people
// aren't going to associate their address with 100's of mayanames
func GetTHORNamesByAddress(ctx context.Context, addr string) (names []string, err error) {
	q := `
		SELECT name
		FROM midgard_agg.mayaname_current_state
		WHERE address = $1 AND last_height() < expire
	`

	rows, err := db.Query(ctx, q, addr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			return nil, err
		}

		names = append(names, name)
	}

	return
}

func GetTHORNamesByOwnerAddress(ctx context.Context, addr string) (names []string, err error) {
	q := `
		SELECT name
		FROM midgard_agg.mayaname_owner_expiration
		WHERE owner = $1 AND last_height() < expire
	`

	rows, err := db.Query(ctx, q, addr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var name string
		if err := rows.Scan(&name); err != nil {
			return nil, err
		}

		names = append(names, name)
	}

	return
}
